module.exports = function (from, to, param, client) {
    return 'A primeira regra do CB é: você não fala sobre o CB.' +
            '\nA segunda regra do CB é: você não fala sobre o CB.' +
            '\nTerceira regra do CB: se alguém gritar "Pára!", fraquejar, abandonar, a luta está terminada.' +
            '\nQuarta regra: ou você é panela ou é frigideira.' + 
            '\nQuinta regra: uma luta de cada vez, pessoal.' +
            '\nSexta regra: sem informações pessoais não autorizadas, a luta é com o personagem do jogo.' +
            '\nSétima regra: as lutas duram o tempo que for necessário, mas se enrolar mais de duas páginas só com mimimi, a moderação pode encerrar o tópico e separar os contendores.' +
            '\nE a oitava e última regra: se este for seu primeiro acesso ao CB, você tem de lutar.';
};